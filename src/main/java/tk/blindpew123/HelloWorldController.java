package tk.blindpew123;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *  Process incoming data from web-form
 *  TODO: imlplement saving to database
 */
@Controller
public class HelloWorldController{

    private GreetingWordsProvider greetingWordsProvider;
    private HelloDAO helloDAO;

    @ResponseBody
    @RequestMapping(value="processWithRequest", method= RequestMethod.POST)
    public void getGreetingsBean(@RequestParam(value="name", defaultValue = "world") String name,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws ServletException, IOException {

        HelloBean bean = new HelloBean();
        bean.setGreetingWord(greetingWordsProvider.getGreeting());
        bean.setName(name);
        Long id = helloDAO.addGreeting(bean);
        bean = helloDAO.getGreetingById(id);
        request.setAttribute("helloBean",bean);
        request.getRequestDispatcher("answer.jsp").forward(request,response);
    }


    public void setGreetingWordsProvider(GreetingWordsProvider greetingWordsProvider) {
        this.greetingWordsProvider = greetingWordsProvider;
    }

    public void setHelloDAO(HelloDAO helloDAO) {
        this.helloDAO = helloDAO;
    }
}

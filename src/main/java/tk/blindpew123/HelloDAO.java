package tk.blindpew123;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public class HelloDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
    @Transactional(readOnly=false)
    public long addGreeting(HelloBean bean) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(bean);
        return bean.getId();
    }
    @Transactional(readOnly=true)
    public HelloBean getGreetingById(long hId){
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "from HelloBean h where h.id = :hId"
        );
        query.setParameter("hId",hId);
        return (HelloBean) query.getSingleResult();
    }

}

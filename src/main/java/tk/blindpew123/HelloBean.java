package tk.blindpew123;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "hello")
public class HelloBean {
    private long id;
    private String name;
    private String greetingWord;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="greeting")
    public String getGreetingWord() {
        return greetingWord;
    }

    public void setGreetingWord(String greetingWord) {
        this.greetingWord = greetingWord;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

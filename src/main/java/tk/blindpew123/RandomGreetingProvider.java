package tk.blindpew123;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomGreetingProvider implements GreetingWordsProvider{
    private final static List<String> greetings = Arrays.asList("Hello", "Howdy", "Hey", "Hi");
    public String getGreeting() {
        return greetings.get((new Random()).nextInt(3));
    }
}

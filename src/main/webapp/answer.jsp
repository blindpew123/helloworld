<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
       <title>Greeting</title>
</head>
<body>
    <c:set var="bean" value="${requestScope.get('helloBean')}" />
    ${bean.greetingWord}&nbsp;${bean.name}
</body>
</html>

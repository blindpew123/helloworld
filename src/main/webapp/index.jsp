<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello App</title>
</head>
<body>
<h2>With whom do you want to say Hello?</h2>
    <form action="processWithRequest" method="POST">
        <input id="nameField" type="text" name="name" />
        <label for="nameField">Name</label>
        <input type="submit" value="OK" />
    </form>
</body>
</html>
